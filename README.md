# Tricoteuses-API-Wikidata

## _API GraphQL pour Tricoteuses basée sur les données ouvertes de Wikidata_

## Utilisation

### Récupération quotidienne des données

Ce script récupère différentes données de [Wikidata](https://www.wikidata.org/). Il les réindente (car par défaut ils tiennent sur une ligne, ce qui ne facilite pas le debogage) :

```bash
cargo run -p tricoteuses_api_wikidata_fetcher -- -c Config.toml -v
```

### Lancement du serveur web

La commande ci-dessous, charge en mémoire tous les fichiers JSON précédentes et lance un serveur web proposant une API GraphQL :

```bash
cargo run -p tricoteuses_api_wikidata -- -c Config.toml -v
```

Pour tester le service GraphQL avec l'interface GraphiQL, ouvrir l'URL `http://localhost:8003` dans un navigateur.
