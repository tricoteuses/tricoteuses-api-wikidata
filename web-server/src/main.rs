#![feature(proc_macro_hygiene, decl_macro)]

extern crate clap;
extern crate env_logger;
extern crate itertools;
#[macro_use]
extern crate juniper;
extern crate juniper_rocket;
#[macro_use]
extern crate rocket;
extern crate rocket_cors;
extern crate serde;
extern crate serde_json;
extern crate tricoteuses_api_wikidata_config as config;
extern crate tricoteuses_api_wikidata_data as data;

use clap::{App, Arg};
use config::Verbosity;
use data::{Context, Depute, ALL_DATASETS};
use juniper::{EmptyMutation, FieldResult, RootNode};
use rocket::http::Method;
use rocket::response::content;
use rocket::State;
use rocket_cors::AllowedOrigins;
use std::path::Path;

struct Query;

graphql_object!(Query: Context |&self| {
    field depute(&executor, id_assemblee_nationale: String) -> FieldResult<Option<&Depute>> {
        Ok(executor.context().get_depute_at_id_assemblee_nationale(&id_assemblee_nationale))
    }

    field deputes(&executor) -> FieldResult<&Vec<Depute>> {
        Ok(&executor.context().deputes)
    }
});

type Schema = RootNode<'static, Query, EmptyMutation<Context>>;

#[get("/")]
fn graphiql() -> content::Html<String> {
    juniper_rocket::graphiql_source("/graphql")
}

#[get("/graphql?<request>")]
fn get_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

#[post("/graphql", data = "<request>")]
fn post_graphql_handler(
    context: State<Context>,
    request: juniper_rocket::GraphQLRequest,
    schema: State<Schema>,
) -> juniper_rocket::GraphQLResponse {
    request.execute(&schema, &context)
}

fn main() {
    let matches = App::new("Tricoteuses API NosDéputes.fr")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data of French Assemblée nationale")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();
    // let data_dir = config_dir.join(&config.data.dir);

    let context = data::load(&config, &config_dir, &ALL_DATASETS, &verbosity);

    // Start Rocket web server.
    let (allowed_origins, failed_origins) = AllowedOrigins::some(
        config
            .api
            .allowed_origins
            .iter()
            .map(String::as_ref)
            .collect::<Vec<&str>>()
            .as_slice(),
    );
    assert!(failed_origins.is_empty());
    let cors = rocket_cors::Cors {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post]
            .into_iter()
            .map(From::from)
            .collect(),
        allow_credentials: true,
        ..Default::default()
    };
    rocket::ignite()
        .attach(cors)
        .manage(context)
        .manage(Schema::new(Query, EmptyMutation::<Context>::new()))
        .mount(
            "/",
            routes![get_graphql_handler, graphiql, post_graphql_handler],
        )
        .launch();
}
