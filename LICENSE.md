> Tricoteuses-API-Wikidata -- A GraphQL server for Wikidata open data
> By: Emmanuel Raviart <emmanuel@raviart.com>
>
> Copyright (C) 2018, 2019 Emmanuel Raviart
> https://framagit.org/tricoteuses/tricoteuses-api-wikidata
>
> Tricoteuses-API-Assemblee is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Tricoteuses-API-Assemblee is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.
