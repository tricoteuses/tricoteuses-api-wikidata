extern crate clap;
extern crate json;
extern crate reqwest;
extern crate slug;
extern crate tricoteuses_api_wikidata_config as config;

use clap::{App, Arg};
use config::Verbosity;
use json::JsonValue;
use slug::slugify;
use std::collections::HashMap;
use std::fs::{self, File};
use std::io::prelude::*;
use std::path::Path;

enum ObjectType {
    Label,
    Literal,
    Uri,
}

struct QueryInfo<'a> {
    object_type: ObjectType,
    statement: &'a str,
    variable_name: &'a str,
}

const DEPUTE_QUERIES_INFOS: &[QueryInfo<'static>] = &[
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P166 ?award_received.",
        variable_name: "award_received",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P69 ?educated_at.",
        variable_name: "educated_at",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P108 ?employer.",
        variable_name: "employer",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P101 ?field_of_work.",
        variable_name: "field_of_work",
    },
    QueryInfo {
        object_type: ObjectType::Uri,
        statement: r"?wikidata wdt:P18 ?image.",
        variable_name: "image",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P463 ?member_of.",
        variable_name: "member_of",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P800 ?notable_work.",
        variable_name: "notable_work",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P106 ?occupation.",
        variable_name: "occupation",
    },
    QueryInfo {
        object_type: ObjectType::Label,
        statement: r"?wikidata wdt:P39 ?position_held.",
        variable_name: "position_held",
    },
    QueryInfo {
        object_type: ObjectType::Literal,
        statement: r"?wikidata wdt:P1045 ?Sycomore_ID.",
        variable_name: "Sycomore_ID",
    },
    QueryInfo {
        object_type: ObjectType::Literal,
        statement: r"?wikidata wdt:P4124 ?Who_s_Who_in_France_biography_ID.",
        variable_name: "Who_s_Who_in_France_biography_ID",
    },
    QueryInfo {
        object_type: ObjectType::Uri,
        statement: r#"
            ?wikipedia schema:about ?wikidata .
            ?wikipedia schema:inLanguage "fr" .
            ?wikipedia schema:isPartOf <https://fr.wikipedia.org/> .
        "#,
        variable_name: "wikipedia",
    },
];

fn main() {
    let matches = App::new("Tricoteuses API Wikidata")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data from Wikidata")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("no-download")
                .short("D")
                .help("No download, merge only"),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();
    let data_dir = config_dir.join(config.data.dir);
    if !data_dir.exists() {
        fs::create_dir(&data_dir).expect("Creation of data directory failed");
    }

    // Query Wikidata.
    let dir_name = "deputes-queries";
    let dir = data_dir.join(dir_name);
    if !matches.is_present("no-download") {
        if dir.exists() {
            fs::remove_dir_all(&dir).expect("Removal of directory failed");
        }
        fs::create_dir(&dir).expect("Creation of directory failed");
        let url = "https://query.wikidata.org/sparql";

        for query_info in DEPUTE_QUERIES_INFOS {
            if verbosity != Verbosity::Verbosity0 {
                println!("Querying Députés' \"{}\"...", query_info.variable_name);
            }
            let query = match query_info.object_type {
                ObjectType::Label => format!(
                    r#"
                        SELECT
                            ?id_assemblee_nationale
                            ?wikidata
                            ?wikidataAltLabel
                            ?wikidataDescription
                            ?wikidataLabel
                            ?{variable_name}
                            ?{variable_name}AltLabel
                            ?{variable_name}Description
                            ?{variable_name}Label
                        WHERE {{
                            SERVICE wikibase:label {{ bd:serviceParam wikibase:language "fr". }}
                            ?wikidata wdt:P4123 ?id_assemblee_nationale.
                            {statement}
                        }}
                    "#,
                    variable_name = query_info.variable_name,
                    statement = query_info.statement,
                ),
                ObjectType::Literal | ObjectType::Uri => format!(
                    r#"
                        SELECT
                            ?id_assemblee_nationale
                            ?wikidata
                            ?wikidataAltLabel
                            ?wikidataDescription
                            ?wikidataLabel
                            ?{variable_name}
                        WHERE {{
                            ?wikidata wdt:P4123 ?id_assemblee_nationale.
                            {statement}
                        }}
                    "#,
                    variable_name = query_info.variable_name,
                    statement = query_info.statement,
                ),
            };
            let text = reqwest::Client::new()
                .post(url)
                .form(&[("format", "json"), ("query", &query)])
                .send()
                .expect("POST request failed")
                .text()
                .expect("receival of POST response failed");
            let json_file_path = dir.join(slugify(query_info.variable_name) + ".json");
            fs::write(&json_file_path, &text).expect("Error while writing JSON file");
        }
    }

    // Merge Wikidata responses.
    let mut depute_by_wikidata_url: HashMap<String, JsonValue> = HashMap::new();
    for entry in fs::read_dir(dir).unwrap() {
        let path = entry.unwrap().path();
        if verbosity != Verbosity::Verbosity0 {
            println!("Merging {}...", path.display());
        }
        let mut json_file = File::open(path).expect("JSON file not found");
        let mut json_text = String::new();
        json_file
            .read_to_string(&mut json_text)
            .expect("error while reading JSON file");
        let data = json::parse(&json_text).expect("invalid JSON");
        for binding in data["results"]["bindings"].members() {
            depute_by_wikidata_url
                .entry(binding["wikidata"]["value"].to_string())
                .and_modify(|depute| {
                    for (key, info) in binding.entries() {
                        if key == "id_assemblee_nationale" {
                            continue;
                        }
                        let new_value = info["value"].clone();
                        let mut existing_value = depute[key].clone();
                        depute[key] = match existing_value {
                            JsonValue::Array(_) => {
                                if existing_value.contains(new_value.clone()) {
                                    existing_value
                                } else {
                                    existing_value.push(new_value).unwrap();
                                    existing_value
                                }
                            }
                            JsonValue::Null => new_value,
                            _ => {
                                if existing_value == new_value {
                                    existing_value
                                } else {
                                    json::array![existing_value.clone(), new_value]
                                }
                            }
                        }
                    }
                })
                .or_insert({
                    let mut depute = JsonValue::new_object();
                    for (key, info) in binding.entries() {
                        depute[key] = info["value"].clone();
                    }
                    depute
                });
        }
    }
    let dir_name = "deputes";
    let dir = data_dir.join(dir_name);
    if dir.exists() {
        fs::remove_dir_all(&dir).expect("Removal of directory failed");
    }
    fs::create_dir(&dir).expect("Creation of directory failed");
    for (_wikidata_url, depute) in depute_by_wikidata_url {
        let json_file_path = dir.join(format!(
            "{}-{}.json",
            slugify(depute["wikidataLabel"].to_string()),
            depute["id_assemblee_nationale"]
        ));
        fs::write(&json_file_path, depute.pretty(2)).expect("Error while writing JSON file");
    }
}
