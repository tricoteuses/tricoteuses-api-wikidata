use crate::serde_utils::str_to_vec;

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Depute {
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "award_receivedAltLabel"
    )]
    pub awards_received_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "award_receivedDescription"
    )]
    pub awards_received_descriptions: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "award_receivedLabel"
    )]
    pub awards_received_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "award_received")]
    pub awards_received_urls: Vec<String>,

    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "educated_atAltLabel"
    )]
    pub educated_at_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "educated_atDescription"
    )]
    pub educated_at_descriptions: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "educated_atLabel")]
    pub educated_at_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "educated_at")]
    pub educated_at_urls: Vec<String>,

    #[serde(default, deserialize_with = "str_to_vec", rename = "employerAltLabel")]
    pub employers_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "employerDescription"
    )]
    pub employers_descriptions: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "employerLabel")]
    pub employers_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "employer")]
    pub employers_urls: Vec<String>,

    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "field_of_workAltLabel"
    )]
    pub fields_of_work_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "field_of_workDescription"
    )]
    pub fields_of_work_descriptions: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "field_of_workLabel"
    )]
    pub fields_of_work_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "field_of_work")]
    pub fields_of_work_urls: Vec<String>,

    pub id_assemblee_nationale: String,

    #[serde(default, deserialize_with = "str_to_vec", rename = "image")]
    pub images_urls: Vec<String>,

    #[serde(default, deserialize_with = "str_to_vec", rename = "member_ofAltLabel")]
    pub member_of_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "member_ofDescription"
    )]
    pub member_of_descriptions: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "member_ofLabel")]
    pub member_of_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "member_of")]
    pub member_of_urls: Vec<String>,

    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "notable_workAltLabel"
    )]
    pub notable_works_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "notable_workDescription"
    )]
    pub notable_works_descriptions: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "notable_workLabel")]
    pub notable_works_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "notable_work")]
    pub notable_works_urls: Vec<String>,

    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "occupationAltLabel"
    )]
    pub occupations_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "occupationDescription"
    )]
    pub occupations_descriptions: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "occupationLabel")]
    pub occupations_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "occupation")]
    pub occupations_urls: Vec<String>,

    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "position_heldAltLabel"
    )]
    pub positions_held_alt_labels: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "position_heldDescription"
    )]
    pub positions_held_descriptions: Vec<String>,
    #[serde(
        default,
        deserialize_with = "str_to_vec",
        rename = "position_heldLabel"
    )]
    pub positions_held_labels: Vec<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "position_held")]
    pub positions_held_urls: Vec<String>,

    #[serde(rename = "Sycomore_ID")]
    pub sycomore_id: Option<String>,

    #[serde(rename = "Who_s_Who_in_France_biography_ID")]
    pub who_is_who_id: Option<String>,

    #[serde(default, deserialize_with = "str_to_vec", rename = "wikidataAltLabel")]
    pub wikidata_alt_labels: Vec<String>,
    #[serde(rename = "wikidataDescription")]
    pub wikidata_description: Option<String>,
    #[serde(rename = "wikidataLabel")]
    pub wikidata_label: String,
    #[serde(rename = "wikidata")]
    pub wikidata_url: String,

    #[serde(rename = "wikipedia")]
    pub wikipedia_url: Option<String>,
}
