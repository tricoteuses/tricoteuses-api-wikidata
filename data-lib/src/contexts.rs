use juniper;
use std::collections::HashMap;

use crate::deputes::Depute;

pub struct Context {
    pub depute_by_id_assemblee_nationale: HashMap<String, *const Depute>,
    pub deputes: Vec<Depute>,
}

impl Context {
    pub fn get_depute_at_id_assemblee_nationale(
        &self,
        id_assemblee_nationale: &str,
    ) -> Option<&Depute> {
        self.depute_by_id_assemblee_nationale
            .get(&id_assemblee_nationale.to_string())
            .map(|depute| unsafe { &*(*depute) })
    }
}

unsafe impl Send for Context {}
unsafe impl Sync for Context {}
impl juniper::Context for Context {}
